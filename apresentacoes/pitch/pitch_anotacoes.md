# Anotações para a apresentação do pitch do projeto

## Problema

Apesar de haver um esforço por parte da RFC para estabelecer um protocolo padrão de troca de dados relacionados a cartões de contatos e este protocolo ser aplicado por empresas como Apple e grupos de desenvolvimento de software livre e de código aberto como o KDE em seus serviços e aplicativos, ainda assim não houve uma tentativa de produzir um programa dedicado somente a gerência de contatos do usuário que atenda a usuários dos três principais sistemas operacionais utilizados atualmente em computadores pessoais (macOS, Windows e Linux).

Os clientes da Apple que usufruem do macOS têm duas grandes possibilidades: os serviços do iCloud ou a assinatura de uma licença do software conhecido como Daylite desenvolvido exclusivamente para o macOS.

Para os usuários do Windows até agora a única possibilidade são os programas da suíte Office da Microsoft, exceto alguns programas desenvolvidos na década de 1990 ou 2000 que estão obsoletos ou não são mais atualizados.

Para as pessoas que usufruem de distribuições Linux, existem várias possibilidades de gerenciar os seus contatos, no entanto somente os desenvolvedores do ambiente de desktop conhecido como KDE produziram um conjunto de ferramentas unificadas sob o aplicativo Kontact e dentre essas ferramentas existe o KAddressBook especializada em manipulação de coleções de contatos, cujo o desenvolvimento prioriza a plataforma Linux, disponibilizando algumas versões para macOS, porém com um suporte precário, e sem lançamentos para o Windows.

## Propósito geral

O SGCon tem a ambição de construir um programa similar ao KAddressBook, mas com suporte para macOS, Windows e Linux, visando:
 - unificação da gerência de contatos do usuário;

 - viabilização ao usuário da liberdade de administrar a sua rede de contatos em qualquer uma das três plataformas de sistemas operacionais principais do mercado; e
 
 - disponibilização da opção de migrar os dados dos contatos para outros serviços ou aplicativos, a fim de flexibilizar a transferência dos dados do acervo de contatos do usuário, garantindo o resgate das informações contidas no acervo independente de plataforma ou serviço.

## Funcionalidades

 1. manipulação do banco de dados de contatos: criar, atualizar e remover contatos assim como prover uma forma de visualizar a coleção de contatos do usuário.
 
 2. importação de contatos: facilitar para o usuário a migração dos dados acumulados de contatos em outros aplicativos e serviços para o SGCon.
 
 3. exportação de contatos: caso o usuário necessite mover os seus contatos para outra plataforma ele terá a liberdade de extrair todos os contatos, ou somente aqueles selecionados previamente, para um formato compreendido por outros programas ou para um formato condizente com o protocolo CardDAV, o qual é um protocolo de comunicação entre cliente e servidor dedicado ao acesso e compartilhamento de dados sobre contatos.
 
 4. designação do tipo de contato: se o contato é uma pessoa ou organização.
 
 5. indicação da natureza ou graus de associação entre os contatos: viabilizar para o usuário uma forma de indicar no perfil do contato qual a sua relação com uma determinada organização (funcionário, presidente, consultor, etc), qual o tipo de relacionamento com outros contatos (pai, filho, colega, esposa, etc).

## Tecnologias

 - para o desenvolvimento da camada de acesso aos dados planejamos utilizar a linguagem python. A motivação dessa escolha foi facilitar o acesso dos integrantes da equipe às ferramentas do processo de desenvolvimento e tentar simplificar a compreensão dos conceitos e métodos envolvidos no processo de desenvolvimento de softwares;
 
 - quanto ao sistema gerenciador de bancos de dados relacionais elegemos o MySQL (na versão 8) a fim de usufruir da biblioteca mysql-connector-python; e
 
 - para construir a camada de interação do usuário com o programa escolhemos a biblioteca flask.

As tecnologias aqui apresentadas foram escolhidas visando uma melhor integração com a linguagem de programação selecionada e evitar o desenvolvimento de um projeto com muitas dependências de pacotes, diminuindo o risco de ocorrerem erros devido a ausência de bibliotecas e simplificar a administração de dependências, evitando a sobrecarga da aprendizagem de várias linguagens de programação simultaneamente.

A PARTE DE TECNOLOGIA ATRIBUÍDA A AGREGAMENTO E IMPORTAÇÃO DE DADOS

DE ACORDO COM ESTE ATAREFAMENTO DO CURSOR UTILIZA-SE DA BUSCA DE CONTATOS

ESTA BUSCA É EFETIVA CASO SE COLOQUE OS CÓDIGOS RELACIONAIS

DADOS NESTE CASO VÃO SER BUSCADOS E EFETUADOS DE FORMA SIMPLES COM A 
LISTA DE CONTATOS

## Riscos

(iii) Proficiência

Será necessário auxiliar os demais membros da equipe quanto à implementação dessas duas bibliotecas (flask e mysql-connector-python) no projeto.
