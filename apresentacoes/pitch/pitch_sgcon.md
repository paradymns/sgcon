---
title: Pitch: SGCon
description: Slides que acompanham o pitch do projeto "SGCon".
header: '***UniCEUB - Eng. de software - Prof. Glauber Boff***'
theme: gaia
paginate: true
backgroundColor: #fff
backgroundImage: url('./recursos/pitch_bg00.jpg')
footer: '***SGCon***'
---

<!-- Estilo global -->
<style>
header {
 color: #67b8e3;
}

h1 {
 color: aqua;
}

p {
 color: #f4f4f4;
}

ul {
 content: "\2022";
 color: #95c9e3;
}

ol {
 content: "\2022";
 color: #95c9e3;
}

footer {
 color: #67b8e3;
}

section::after {
 content: attr(data-marpit-pagination) ' / ' attr(data-marpit-pagination-total);
 font-weight: bold;
 color: #67b8e3;
}
</style>

<!-- Estilo local -->
<style scoped>
h1 {
 text-align: center;
}

p {
 text-align: center;
}
</style>

# SGCon

![bg left:40% 80%](./recursos/sgcon_icone.png)

**Sistema Gerenciador de Contatos**

**Responsáveis**: Andreas Ramalho Spyridakis, Pedro Figuerêdo de Oliveira Freitas, Rodrigo Fernandes O'Donnell Timm e Guilherme Santana Soares

[GitLab - sgcon](https://gitlab.com/paradymns/sgcon)

---

<!-- backgroundImage: url('./recursos/pitch_bg01.jpg') -->

# Problema

Dispersão da rede de contatos do usuário em diversos serviços/programas de diversos dispositivos/sistemas operacionais.

---

# Propósito geral

Os objetivos almejados com o desenvolvimento do SGCon são:
 - centralizar a gerência de contatos do usuário;

 - não prender o usuário a um sistema operacional, programa ou serviço específico; e

 - facilitar a migração dos dados de contatos para serviços ou programas de terceiros.

---

# Funcionalidades

Os quatro principais requisitos necessários para a criação de um sistema de gerência de contatos funcional são:
 1. manipulação de um banco de dados de contatos;

 2. importação dos contatos de serviços ou programas de terceiros;
 
---

# Funcionalidades (*cont.*)

 3. exportação todos os contatos, ou aqueles pré-selecionados, para um formato suportado por outros serviços ou programas;
 
 4. classificação do contato: pessoa ou organização; e

 5. estabelecimento de tipos ou graus de associações entre os contatos: (i) tipos de vínculo a uma organização (vínculo profissional); (ii) tipos de relacionamento entre pessoas (e.g. parentesco).
 
---

# Tecnologias

Tecnologias que podem ser empregadas para o desenvolvimento desse projeto:
 - back-end: python;

 - banco de dados: MySQL 8 (`mysql-connector-python`);

 - front-end: HTML e CSS (`flask`).

---

# Riscos

Projeto tem que ser condizente com as seguintes variáveis:
 - tempo;

 - proficiência;

 - experiência.

---

# Riscos (*cont.*)

(i) Tempo
 - período de 6 meses para o desenvolvimento do programa

 - possibilidade de implementar 6 funcionalidades até o prazo de entrega final
 
---

# Riscos (*cont.*)

(ii) Proficiência
 - familiaridade dos desenvolvedores com as tecnologias elegidas
 
Os integrantes da equipe conhecem python, no entanto dois integrantes tiveram a oportunidade de utilizar os pacotes mysql-connector-python ou flask.

Metade da equipe teve um contato breve ou nenhuma experiência com SQL.
 
---

# Riscos (*cont.*)

(iii) Experiência
 - conhecimento prático dos integrantes com o processo de desenvolvimento de software
 
---

<!-- Estilo local -->
<style scoped>
a {
 color: #95c9e3;
}
</style>

# Mais informações

**E-mails**:
 - andreas.rs@sempreceub.com
 - rodrigo.timm@sempreceub.com
 - pedro.ff@sempreceub.com
 - guilherme.soares@sempreceub.com

**Repositório**: https://gitlab.com/paradymns/sgcon
