<div align="center">
<h1>SGCon</h1>
<img width="200" src="./recursos/sgcon_logo.png" alt="Logo do SGCon">
<p>
<b>Responsáveis</b>: <i>Andreas Ramalho Spyridakis</i> - <i>Gabriel Silva Ramos</i> - <i>Guilherme Campos Santos</i> - <i>Pedro Figuerêdo de Oliveira Freitas</i> - <i>Rodrigo Fernandes O'Donnell Timm</i>
</p>
</div>

# Sumário

## Descrição

SGCon é abreviação de Sistema Gerenciador de Contatos. É um programa concebido com a intenção de auxiliar o usuário a administrar a sua rede de contatos.

Esse é um projeto desenvolvido por alunos de graduação em ciência da computação do [UniCEUB](https://www.uniceub.br/) (Centro Universitário de Brasília) para a disciplina de engenharia de software ministrada pelo professor Glauber Boff no segundo semestre de 2021.

## Responsáveis

 - Andreas Ramalho Spyridakis
 - Gabriel Silva Ramos
 - Guilherme Campos Santos
 - Pedro Figuerêdo de Oliveira Freitas
 - Rodrigo Fernandes O'Donnell Timm

## Observações

### Slides da apresentação do pitch

Se encontram dentro do diretório `apresentacoes > pitch`, é o arquivo `01_apresentacao-Pitch_SGCon`.

## Recursos e tecnologias utilizadas

 - Python
 - MySQL (por meio do pacote python "mysql-connector-python")
 - HTML e CSS (por meio do pacote Flask)
 - [Marp](https://marp.app/) (para produzir as apresentações)
 - [Markdown](https://www.markdownguide.org/) (especificação [CommonMark](https://commonmark.org/) para gerar a documentação)

## Mais informações

### Contatos

 - *Andreas Ramalho Spyridakis* <andreas.rs@sempreceub.com>
 - *Gabriel Silva Ramos* <gabriel.sramos@sempreceub.com>
 - *Guilherme Campos Santos* <guilherme.campos@sempreceub.com>
 - *Pedro Figuerêdo de Oliveira Freitas* <pedro.ff@sempreceub.com>
 - *Rodrigo Fernandes O'Donnell Timm* <rodrigo.timm@sempreceub.com>
